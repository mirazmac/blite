var obj = {
    'abc' : {
        'name' : 'ABC Radio',
        'stream' : 'http://st2.zenorad.io:14424/;stream.mp3'
    },
    'betar' : {
        'name' : 'Bangladesh Betar',
        'stream' : 'http://192.235.87.105:13748/;stream.mp3'
    },
    'colours' : {
        'name' : 'Colours',
        'stream' : 'http://50.22.212.196:8156/;stream.mp3'
    },
    'dhakafm' : {
        'name' : 'Dhaka FM',
        'stream' : 'http://118.179.219.244:8000/;stream.mp3'
    },
    'jagofm' : {
        'name' : 'Jago FM',
        'stream' : 'http://192.240.102.3:9099/;stream.mp3'
    },
    'peoples' : {
        'name' : 'Peoples Radio',
        'stream' : '"http://s3.myradiostream.com:14498/;stream.mp3'
    },
    '' : {
        'name' : 'Radio Bhumi',
        'stream' : 'http://shaincast.caster.fm:21340/listen.mp3?authna456d2070577d82863a25d17018cca2a'
    },
    '' : {
        'name' : '',
        'stream' : ''
    },
    '' : {
        'name' : '',
        'stream' : ''
    },
    '' : {
        'name' : '',
        'stream' : ''
    },

};

function onError(error)
{
    console.log(error.message);
}

function onConfirmRetry(button) {
    if (button == 1) {
        html5audio.play();
    }
}

function pad2(number) {
    return (number < 10 ? '0' : '') + number
}
